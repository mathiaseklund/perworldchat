package me.mathiaseklund.pwc;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.google.common.collect.Lists;

public class Config {

	Main main = Main.getMain();
	File f;
	FileConfiguration fc;

	HashMap<String, List<String>> linked = new HashMap<>();

	public Config() {
		main = Main.getMain();
		f = new File(main.getDataFolder(), "config.yml");
		if (!f.exists()) {
			main.saveResource("config.yml", true);
		}
		fc = YamlConfiguration.loadConfiguration(f);

		load();
	}

	void load() {
		ConfigurationSection section = fc.getConfigurationSection("linked");
		if (section != null) {
			for (String world : section.getKeys(false)) {
				linked.put(world, section.getStringList(world));
			}
		}
	}

	public List<String> getLinked(String world) {
		List<String> l = Lists.newArrayList();
		for (String s : linked.keySet()) {
			List<String> list = linked.get(s);
			if (list.contains(world)) {
				l.addAll(list);
			}
		}

		return l;
	}
}

package me.mathiaseklund.pwc;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Lists;

public class Main extends JavaPlugin {

	static Main main;
	Config cfg;
	public List<String> global = Lists.newArrayList();

	public static Main getMain() {
		return main;
	}

	public void onEnable() {
		main = this;

		cfg = new Config();
		
		loadListener();
		loadCommands();
	}

	public Config getCfg() {
		return cfg;
	}

	void loadListener() {
		Bukkit.getPluginManager().registerEvents(new Listeners(), main);
	}

	void loadCommands() {
		getCommand("global").setExecutor(new GlobalCommand());
	}
}

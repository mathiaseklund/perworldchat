package me.mathiaseklund.pwc;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class GlobalCommand implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {

		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (sender.hasPermission("pwc.global")) {
				if (args.length > 0) {
					String message = null;
					for (String s : args) {
						if (message == null) {
							message = s;
						} else {
							message = message + " " + s;
						}
					}

					main.global.add(sender.getName());

					player.chat(message);

				} else {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eUsage:&7 /global [message..]"));
				}
			}
		}

		return true;
	}

}

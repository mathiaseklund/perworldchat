package me.mathiaseklund.pwc;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.google.common.collect.Lists;

public class Listeners implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		String sourceWorld = player.getWorld().getName();
		List<String> linked = main.getCfg().getLinked(sourceWorld);
		if (main.global.contains(player.getName())) {
			main.global.remove(player.getName());
		} else {
			List<Player> recipients = Lists.newArrayList(event.getRecipients());
			for (Player p : recipients) {
				if (p.getUniqueId() != player.getUniqueId()) {
					String world = p.getWorld().getName();
					if (world.equals(sourceWorld) || linked.contains(world)) {
					} else {
						if (!p.hasPermission("pwc.bypass")) {
							event.getRecipients().remove(p);
						}
					}
				}
			}
		}
	}
}
